extends RigidBody


const REPLAY = true


var plunger_strength = 0.0

var replay = []


func _ready():
	var file = File.new()
	file.open("res://replay_test.txt", File.READ)

	while not file.eof_reached():
		replay.append(file.get_line())

func _get_plunger_strength() -> float:
	if REPLAY:
		var next_line = replay.pop_front()
		while next_line != null and next_line.split(" ")[0] != "plunger":
			next_line = replay.pop_front()

		if next_line == null:
			return 0.0

		plunger_strength = float(next_line.split(" ")[1])

		return plunger_strength
	else:
		plunger_strength = Input.get_action_strength("plunger")
		print("plunger " + str(plunger_strength))
		return plunger_strength


func _physics_process(_delta):
	# Push down on the plunger.
	add_central_force(-global_transform.basis.y * (_get_plunger_strength() * 96000.0))
