extends Area


signal ball_eaten


func _on_body_entered(body):
	if body.is_in_group("ball"):
		body.queue_free()
		emit_signal("ball_eaten")
